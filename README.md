# README

1. prerequisites
    * `java 10` or newer with `apache maven` 3.3
    * `nodejs` 10 or newer, `@angular/cli` 6 or newer, `npm` 6 or newer
    * `mysql server` (db user and connection string configurable through `application.properies`)

2. how to run 
    * in api folder
        * create a database and user, fill data into `application.properies`. (you can also set admin password)
        * in `application.properies` set line to `spring.jpa.hibernate.ddl-auto=create`
        * in api folder run command `mvn spring-boot:run` it should download all packages, create tables, and run api. After that you should change this line back to `spring.jpa.hibernate.ddl-auto=validate`
    * in front folder        
        * run command `npm i`
        * if you had installed `@angular/cli` globally run command `ng serve` and skip next step
        * if you havent installed `@angular/cli` and  dont want to install `@angular/cli` globally run `npm run-script ng serve` to run 
        * the application should be accessible under url `localhost:4200`

3. tips and general knowledge
    * admin panel is accessible under url `localhost:4200/admin` password is in `application.properies`