import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy
} from "@angular/core";
import { Post } from "../../../../shared/models/post.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostComponent implements OnInit {
  @Input()
  post: Post;
  constructor(private router: Router) {}

  ngOnInit() {}

  goToDetails() {
    this.router.navigate(["post", this.post.id]);
  }
}
