import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PostComponent } from "./post-list/post/post.component";
import { PostListComponent } from "./post-list/post-list.component";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { SharedModule } from "src/app/shared/shared.module";
import { MatCardModule } from "@angular/material/card";
import { BlurImageDirective } from "./post-list/blur-image.directive";
import { AdsenseComponent } from "./adsense/adsense.component";

@NgModule({
  declarations: [
    PostListComponent,
    PostComponent,
    BlurImageDirective,
    AdsenseComponent
  ],
  imports: [CommonModule, InfiniteScrollModule, SharedModule, MatCardModule],
  exports: [PostListComponent, AdsenseComponent]
})
export class WebsiteSharedModule {}
