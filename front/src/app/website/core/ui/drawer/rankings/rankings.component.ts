import {
  Component,
  OnInit,
  Renderer2,
  ViewChild,
  ElementRef
} from "@angular/core";
import { Observable } from "rxjs";
import { RankingService } from "../_state/ranking.service";

@Component({
  selector: "blog-rankings",
  templateUrl: "./rankings.component.html",
  styleUrls: ["./rankings.component.scss"]
})
export class RankingsComponent implements OnInit {
  //TODO: moze to przeniesc do componentu wyzej
  ranking$: Observable<any>;

  constructor(
    private rankingsService: RankingService,
    private renderer2: Renderer2
  ) {}

  ngOnInit() {
    this.rankingsService.fetchRankings();
    this.ranking$ = this.rankingsService.ranking$;
  }

}
