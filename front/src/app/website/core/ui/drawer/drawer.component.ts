import { Component, OnInit } from "@angular/core";
import { UIStateService } from "../_state/ui-state.service";
import { TouchSequence } from "selenium-webdriver";

@Component({
  selector: "app-drawer",
  templateUrl: "./drawer.component.html",
  styleUrls: ["./drawer.component.scss"]
})
export class DrawerComponent implements OnInit {
  isDrawerOpen$ = this.uiStateService.isDrawerOpen$;
  links: Link[] = [
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"]),
    new Link("test", ["/"])
  ];

  constructor(public uiStateService: UIStateService) {}

  ngOnInit() {}

  closeDrawer() {
    this.uiStateService.switchDrawer(false);
  }
}

class Link {
  label: string;
  routerLink: string[];

  constructor(label: string, routerLink: string[]) {
    this.label = label;
    this.routerLink = routerLink;
  }
}
