import { ID } from "@datorama/akita";

export class Ranking {
  id: ID;
  mostPopularTags: TagRanking[];
  mostPopularPosts: PostRanking[];
  bestRatedPosts: PostRanking[];
}

export interface TagRanking {
  id: number;
  name: string;
}

export interface PostRanking {
  id: number;
  title: string;
}
