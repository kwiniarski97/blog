import { Injectable, OnDestroy } from "@angular/core";
import { ID } from "@datorama/akita";
import { HttpClient } from "@angular/common/http";
import { Ranking } from "./ranking.model";
import { ReplaySubject, Subject } from "rxjs";
import { environment } from "src/environments/environment";
import { takeUntil } from "rxjs/operators";

@Injectable({ providedIn: "root" })
export class RankingService implements OnDestroy {
  private ranking = new ReplaySubject<Ranking>();
  ranking$ = this.ranking.asObservable();

  private destroy$ = new Subject<void>();
  constructor(private http: HttpClient) {}

  ngOnDestroy() {
    this.destroy$.next();
  }

  fetchRankings() {
    this.http
      .get(`${environment.apiUrl}/rankings`)
      .pipe(takeUntil(this.destroy$))
      .subscribe((ranking: Ranking) => this.ranking.next(ranking));
  }
}
