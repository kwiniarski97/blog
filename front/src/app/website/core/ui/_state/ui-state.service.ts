import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";

@Injectable()
export class UIStateService {
  private isDrawerOpen = new BehaviorSubject(false);
  isDrawerOpen$ = this.isDrawerOpen.asObservable();

  private searchEv = new Subject<string>();

  searchEvent$ = this.searchEv.asObservable();

  switchDrawer(state?: boolean) {
    this.isDrawerOpen.next(state != null ? state : !this.isDrawerOpen.value);
  }

  search(query: string) {
    this.searchEv.next(query);
  }
}
