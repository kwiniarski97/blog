import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeaderComponent } from "./ui/header/header.component";
import { SharedModule } from "../../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { HamburgerButtonComponent } from "./ui/common/hamburger-button/hamburger-button.component";
import { DrawerComponent } from "./ui/drawer/drawer.component";
import { MatSidenavModule } from "@angular/material/sidenav";
import { UIStateService } from "./ui/_state/ui-state.service";
import { RouterModule } from "@angular/router";
import { RankingsComponent } from "./ui/drawer/rankings/rankings.component";
import { MatListModule } from "@angular/material/list";
import { MatExpansionModule } from "@angular/material/expansion";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    MatSidenavModule,
    RouterModule,
    MatListModule,
    MatExpansionModule
  ],
  declarations: [
    HeaderComponent,
    HamburgerButtonComponent,
    DrawerComponent,
    RankingsComponent,
  ],
  exports: [HeaderComponent, DrawerComponent, MatSidenavModule],
  providers: [UIStateService]
})
export class CoreModule {}
