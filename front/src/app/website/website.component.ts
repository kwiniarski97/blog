import { Component } from "@angular/core";
import { UIStateService } from "./core/ui/_state/ui-state.service";

@Component({
  selector: "app-website",
  templateUrl: "./website.component.html",
  styleUrls: ["./website.component.scss"]
})
export class WebsiteComponent {
  isDrawerOpen$ = this.uiStateService.isDrawerOpen$;
  constructor(private uiStateService: UIStateService) {}

  closeDrawer() {
    this.uiStateService.switchDrawer(false);
  }
}
