import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PostDetailComponent } from "./posts/post-detail/post-detail.component";
import { WebsiteComponent } from "./website.component";
import { PostsModule } from "./posts/posts.module";
import { SharedModule } from "../shared/shared.module";
import { CoreModule } from "./core/core.module";
import { RecentComponent } from "./posts/recent/recent.component";
import { WebsiteSharedModule } from "./common/website-shared.module";
import { SearchResultsComponent } from "./posts/search-results/search-results.component";
import { TagViewComponent } from "./posts/tag-view/tag-view.component";

const routes: Routes = [
  {
    path: "",
    component: WebsiteComponent,
    children: [
      { path: "", component: RecentComponent },
      { path: "post/:id", component: PostDetailComponent },
      { path: "search", component: SearchResultsComponent },
      { path: "tag/:id", component: TagViewComponent }
    ]
  }
];

@NgModule({
  imports: [
    SharedModule,
    CoreModule,
    PostsModule,
    WebsiteSharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WebsiteComponent],
  exports: [RouterModule]
})
export class WebsiteModule {}
