import { Component, OnInit } from "@angular/core";
import { PostsService } from "../_state/posts.service";
import { PostsQuery } from "../_state/posts.query";
import { Observable } from "rxjs";
import { map, filter } from "rxjs/operators";
import { Post } from "src/app/shared/models/post.model";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "blog-tag-view",
  templateUrl: "./tag-view.component.html",
  styleUrls: ["./tag-view.component.scss"]
})
export class TagViewComponent implements OnInit {
  isLoading$: Observable<boolean>;
  selectedTagId: number;
  posts$: Observable<Post[]>;
  constructor(
    private postsQuery: PostsQuery,
    private postsService: PostsService,
    private activatedRoute: ActivatedRoute
  ) {
    this.postsService.resetPaging();
  }

  ngOnInit() {
    this.selectedTagId = +this.activatedRoute.snapshot.paramMap.get("id");

    this.isLoading$ = this.postsQuery.selectLoading();
    this.fetchData();
  }

  scroll() {
    this.fetchData();
  }

  private fetchData() {
    this.postsService.getByTagId(this.selectedTagId, this.postsQuery.getPage());
    this.posts$ = this.postsQuery
      .selectAll()
      .pipe(
        map(posts =>
          posts.filter(
            p => p.tags && !!p.tags.find(t => t.id == this.selectedTagId)
          )
        )
      );
  }
}
