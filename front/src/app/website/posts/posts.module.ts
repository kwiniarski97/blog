import { NgModule } from "@angular/core";
import { PostsQuery } from "./_state/posts.query";
import { PostsStore } from "./_state/posts.store";
import { PostsService } from "./_state/posts.service";
import { PostDetailComponent } from "./post-detail/post-detail.component";
import { SharedModule } from "src/app/shared/shared.module";
import { PostDetailScreenComponent } from "./post-detail/post-detail-screen/post-detail-screen.component";
import { PostChipsComponent } from "./post-detail/post-chips/post-chips.component";
import { MatChipsModule } from "@angular/material/chips";
import { NgxCommentoModule } from "ngx-commento";
import { RecentComponent } from "./recent/recent.component";
import { WebsiteSharedModule } from "../common/website-shared.module";
import { SearchResultsComponent } from './search-results/search-results.component';
import { TagViewComponent } from './tag-view/tag-view.component';
import { UpvoteButtonComponent } from './post-detail/upvote-button/upvote-button.component';

@NgModule({
  imports: [
    SharedModule,
    MatChipsModule,
    NgxCommentoModule,
    WebsiteSharedModule
  ],
  declarations: [
    PostDetailComponent,
    PostDetailScreenComponent,
    PostChipsComponent,
    RecentComponent,
    SearchResultsComponent,
    TagViewComponent,
    UpvoteButtonComponent,
  ],
  providers: [PostsQuery, PostsStore, PostsService]
})
export class PostsModule {}
