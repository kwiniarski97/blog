import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { Post } from "../../../shared/models/post.model";
import { PostsModule } from "../posts.module";
import { PostsState } from "./posts.state";
import { PostsStore } from "./posts.store";

@Injectable()
export class PostsQuery extends QueryEntity<PostsState, Post> {
  constructor(protected store: PostsStore) {
    super(store);
  }

  getLast() {
    return this.getSnapshot().last;
  }

  getPage() {
    return this.getSnapshot().number;
  }

  getById(id: number) {
    return this.selectEntity(id);
  }

  getSearchValue() {
    return this.getSnapshot().searchValue;
  }
}
