import { EntityState } from "@datorama/akita";
import { Post } from "../../../shared/models/post.model";
import { PaginatedResponse } from "src/app/shared/models/paginated-reponse.model";

export interface PostsState
  extends EntityState<Post>,
    PaginatedResponse<Post> {}
