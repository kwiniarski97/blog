import { Injectable, OnDestroy } from "@angular/core";
import { PostsModule } from "../posts.module";
import { environment } from "src/environments/environment";
import { Post } from "../../../shared/models/post.model";
import { PaginatedResponse } from "src/app/shared/models/paginated-reponse.model";
import { delay, takeUntil, retryWhen, timeout } from "rxjs/operators";
import { PostsStore } from "./posts.store";
import { transaction } from "@datorama/akita";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject } from "rxjs";
import { UrlSerializer } from "@angular/router";

@Injectable()
export class PostsService implements OnDestroy {
  private destroy$ = new Subject<void>();

  private apiURL = `${environment.apiUrl}/posts/`;

  private upvotedKey = "upvoted";
  constructor(private postsStore: PostsStore, private http: HttpClient) {}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getRecent(page: number) {
    this.postsStore.setLoading(true);
    this.http
      .get<any>(`${this.apiURL}/recent/${page}`)
      .pipe(
        timeout(4999),
        takeUntil(this.destroy$),
        retryWhen(error => error.pipe(delay(5000)))
      )
      .subscribe(res => {
        this.updatePosts(res);
      });
  }

  getById(id: number) {
    this.postsStore.setLoading(true);
    this.http
      .get<Post>(`${this.apiURL}/${id}`)
      .pipe(
        takeUntil(this.destroy$),
        retryWhen(error => error.pipe(delay(5000)))
      )
      .subscribe(res => this.updatePost(res));
  }

  getByTagId(selectedTagId: number, page: number = 0) {
    this.postsStore.setLoading(true);
    this.http
      .get<PaginatedResponse<Post>>(
        `${this.apiURL}/tag/${selectedTagId}/${page}`
      )
      .pipe(
        takeUntil(this.destroy$),
        retryWhen(error => error.pipe(delay(5000)))
      )
      .subscribe(res => this.updatePosts(res));
  }

  search(query: string, page: number = 0) {
    this.postsStore.setSearchValue(query);
    if (!query) {
      return;
    }
    this.postsStore.setLoading(true);
    this.http
      .get<PaginatedResponse<Post>>(
        `${this.apiURL}/search/${page}?query=${query}`
      )
      .pipe(
        takeUntil(this.destroy$),
        retryWhen(error => error.pipe(delay(5000)))
      )
      .subscribe(res => {
        this.postsStore.setSearchValue(query);
        this.updatePosts(res);
      });
  }

  isUpvoted(id: number): boolean {
    return this.getUpvotedIdsArray().includes(id);
  }

  upvote(id: number): void {
    this.http
      .post(`${this.apiURL}/upvote/${id}`,"")
      .pipe(takeUntil(this.destroy$))
      .subscribe((score) => {
        this.postsStore.update(id, {score: score})
        this.saveToUpvotedArray(id);
      });
  }

  private getUpvotedIdsArray(): number[] {
    const localStorageEntry = JSON.parse(localStorage.getItem(this.upvotedKey));
    return localStorageEntry || [];
  }

  private saveToUpvotedArray(id: number) {
    const currentArray = this.getUpvotedIdsArray();
    currentArray.push(id);
    localStorage.setItem(this.upvotedKey, JSON.stringify(currentArray));
  }

  @transaction()
  private updatePosts(res: PaginatedResponse<Post>) {
    const nextPage = res.number + 1;
    this.postsStore.add(res.content);
    this.postsStore.updatePage({ last: res.last, number: nextPage });
    this.postsStore.setLoading(false);
  }

  @transaction()
  private updatePost(res: Post) {
    this.postsStore.createOrReplace(res.id, res);
    this.postsStore.setLoading(false);
  }

  @transaction()
  public resetPaging() {
    this.postsStore.updatePage({ last: false, number: 0 });
  }
}
