import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input
} from "@angular/core";
import { Tag } from "src/app/shared/models/tag.model";

@Component({
  selector: "blog-post-chips",
  templateUrl: "./post-chips.component.html",
  styleUrls: ["./post-chips.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostChipsComponent implements OnInit {
  @Input()
  tags: Tag[];
  constructor() {}

  ngOnInit() {}

  trackFn(item: Tag, index) {
    return item.id;
  }
}
