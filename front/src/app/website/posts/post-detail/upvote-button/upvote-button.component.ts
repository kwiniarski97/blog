import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  selector: "blog-upvote-button",
  templateUrl: "./upvote-button.component.html",
  styleUrls: ["./upvote-button.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpvoteButtonComponent implements OnInit {
  @Input()
  isUpvoted = false;

  @Input()
  voteCount: number;

  @Output()
  upvoted = new EventEmitter<void>();
  constructor() {}

  ngOnInit() {}

  upvote() {
    this.upvoted.emit();
  }
}
