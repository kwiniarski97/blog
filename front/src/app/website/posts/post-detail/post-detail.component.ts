import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import { Post } from "../../../shared/models/post.model";
import { ActivatedRoute } from "@angular/router";
import { PostsService } from "../_state/posts.service";
import { PostsQuery } from "../_state/posts.query";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-post-detail",
  templateUrl: "./post-detail.component.html",
  styleUrls: ["./post-detail.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostDetailComponent implements AfterViewInit, OnDestroy {
  private id: number;
  post$: Observable<Post>;
  isLoading$: Observable<boolean>;
  scriptLoaded = new Subject();
  isUpvoted: boolean;
  constructor(
    private route: ActivatedRoute,
    private postsQuery: PostsQuery,
    private postsService: PostsService
  ) {
    this.isLoading$ = this.postsQuery.selectLoading();
    this.id = +this.route.snapshot.paramMap.get("id");

    this.fetchPost();
    this.checkIfAlreadyUpvoted();
  }

  private fetchPost() {
    this.postsService.getById(this.id);
    this.post$ = this.postsQuery.getById(this.id);
  }

  private checkIfAlreadyUpvoted() {
    this.isUpvoted = this.postsService.isUpvoted(this.id);
  }

  ngAfterViewInit() {}

  ngOnDestroy() {}

  upvote() {
    this.postsService.upvote(this.id);
    this.isUpvoted = true;
    this.post$ = this.postsQuery.getById(this.id);
  }
}
