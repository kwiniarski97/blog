import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { CoreModule } from "./core/core.module";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { TokenInterceptor } from "./admin/core/token.interceptor";

const angularModules = [
  BrowserModule,
  BrowserAnimationsModule,
  HttpClientModule
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    ...angularModules,
    AppRoutingModule,
    CoreModule,
    MatSnackBarModule,
    MatDatepickerModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
