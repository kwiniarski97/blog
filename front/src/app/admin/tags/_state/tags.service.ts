import { Injectable, OnDestroy } from "@angular/core";
import { ID } from "@datorama/akita";
import { HttpClient } from "@angular/common/http";
import { TagsStore } from "./tags.store";
import { Tag } from "../../../shared/models/tag.model";
import { environment } from "src/environments/environment";
import { Subject, Observable } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { SnackBarService } from "src/app/shared/snack-bar.service";

@Injectable()
export class TagsService implements OnDestroy {
  private destroy$ = new Subject<void>();

  private apiURL = `${environment.apiUrl}/tags/`;

  constructor(
    private tagsStore: TagsStore,
    private http: HttpClient,
    private snackBar: SnackBarService
  ) {}

  ngOnDestroy() {
    this.destroy$.next();
  }

  get() {
    this.http
      .get(this.apiURL)
      .pipe(takeUntil(this.destroy$))
      .subscribe((tags: Tag[]) => this.tagsStore.set(tags));
  }

  add(tag: Tag) {
    this.http
      .post(`${this.apiURL}/create`, tag)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (id: number) => {
          tag.id = id;
          this.tagsStore.add(tag);
        },
        () => {
          this.showError();
        }
      );
  }

  update(id: number, tag: Partial<Tag>) {
    this.tagsStore.update(id, tag);
  }

  remove(id: ID) {
    this.http
      .delete(`${this.apiURL}/delete/${id}`)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        () => {
          this.tagsStore.remove(id);
        },
        error => {
          this.showError(error.message);
        }
      );
  }

  private showError(message: string = "Wystąpił błąd") {
    this.snackBar.alert(message);
  }
}
