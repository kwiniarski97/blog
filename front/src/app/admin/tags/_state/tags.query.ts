import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { TagsStore, TagsState } from "./tags.store";
import { Tag } from "../../../shared/models/tag.model";

@Injectable()
export class TagsQuery extends QueryEntity<TagsState, Tag> {
  constructor(protected store: TagsStore) {
    super(store);
  }
}
