import { Injectable } from "@angular/core";
import { EntityState, EntityStore, StoreConfig } from "@datorama/akita";
import { Tag } from "../../../shared/models/tag.model";

export interface TagsState extends EntityState<Tag> {}

@Injectable()
@StoreConfig({ name: "admin-tags" })
export class TagsStore extends EntityStore<TagsState, Tag> {
  constructor() {
    super();
  }
}
