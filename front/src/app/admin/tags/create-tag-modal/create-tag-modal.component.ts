import { Component, OnInit } from "@angular/core";

@Component({
  selector: "blog-create-tag-modal",
  templateUrl: "./create-tag-modal.component.html",
  styleUrls: ["./create-tag-modal.component.scss"]
})
export class CreateTagModalComponent implements OnInit {
  name: string;

  constructor() {}

  ngOnInit() {}
}
