import { NgModule } from "@angular/core";

import { TagsComponent } from "./tags.component";
import { TagsListComponent } from "./tags-list/tags-list.component";
import { TagsQuery } from "./_state/tags.query";
import { TagsService } from "./_state/tags.service";
import { TagsStore } from "./_state/tags.store";
import { CreateTagModalComponent } from "./create-tag-modal/create-tag-modal.component";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "src/app/shared/shared.module";
import { MatDialogModule } from "@angular/material/dialog";
import { MatCardModule } from "@angular/material/card";

@NgModule({
  imports: [SharedModule, FormsModule, MatDialogModule, MatCardModule],
  declarations: [TagsComponent, TagsListComponent, CreateTagModalComponent],
  providers: [TagsQuery, TagsService, TagsStore],
  entryComponents: [CreateTagModalComponent]
})
export class TagsModule {}
