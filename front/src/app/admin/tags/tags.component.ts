import { Component, OnInit, OnDestroy } from "@angular/core";
import { ID } from "@datorama/akita";
import { Observable, Subject } from "rxjs";
import { Tag } from "src/app/shared/models/tag.model";
import { TagsQuery } from "./_state/tags.query";
import { TagsService } from "./_state/tags.service";
import { MatDialog } from "@angular/material/dialog";
import { CreateTagModalComponent } from "./create-tag-modal/create-tag-modal.component";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "blog-tags",
  templateUrl: "./tags.component.html",
  styleUrls: ["./tags.component.scss"]
})
export class TagsComponent implements OnInit, OnDestroy {
  tags$: Observable<Tag[]>;

  isLoading$: Observable<boolean>;

  destroy$ = new Subject();

  constructor(
    private tagsQuery: TagsQuery,
    private tagsService: TagsService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.tagsService.get();
    this.isLoading$ = this.tagsQuery.selectLoading();
    this.getTags();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  newTag() {
    const dialogRef = this.dialog.open(CreateTagModalComponent, {
      closeOnNavigation: true
    });
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroy$))
      .subscribe(result => {
        if (result) {
          this.saveTag(result);
          this.getTags();
        }
      });
  }

  private saveTag(name: string) {
    const tag = new Tag();
    tag.name = name;
    this.tagsService.add(tag);
  }

  private getTags() {
    this.tags$ = this.tagsQuery.selectAll();
  }

  delete(id: number) {
    if (confirm(`Czy napeno chcesz usunąć taga o id = ${id}?`)) {
      this.tagsService.remove(id);
    }
  }
}
