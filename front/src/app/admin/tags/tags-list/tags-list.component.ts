import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TagsService } from "../_state/tags.service";
import { Observable } from "rxjs";
import { Tag } from "src/app/shared/models/tag.model";
import { TagsQuery } from "../_state/tags.query";

@Component({
  selector: "blog-tags-list",
  templateUrl: "./tags-list.component.html",
  styleUrls: ["./tags-list.component.scss"]
})
export class TagsListComponent implements OnInit {
  @Input()
  tags: Tag[];

  @Output()
  delete = new EventEmitter<number>();
  constructor() {}

  ngOnInit() {}

  deleteClicked(id: number) {
    this.delete.emit(id);
  }
}
