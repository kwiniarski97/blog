import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { PostsState } from "../bulk/posts.store";
import { Post } from "src/app/shared/models/post.model";
import { PostsDetailStore } from "./post-details.store";

@Injectable()
export class PostsDetailQuery extends QueryEntity<PostsState, Post> {
  constructor(protected store: PostsDetailStore) {
    super(store);
  }
}