import { Injectable } from "@angular/core";
import { StoreConfig, EntityStore, EntityState } from "@datorama/akita";
import { Post } from "src/app/shared/models/post.model";

export interface PostsDetailState extends EntityState<Post> {}

@Injectable()
@StoreConfig({ name: "adm-posts-detail" })
export class PostsDetailStore extends EntityStore<PostsDetailState, Post> {
  constructor() {
    super();
  }
}
