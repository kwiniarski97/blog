import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { PostsStore, PostsState } from "./posts.store";
import { Post } from "../../../../shared/models/post.model";

@Injectable()
export class PostsQuery extends QueryEntity<PostsState, Post> {
  constructor(protected store: PostsStore) {
    super(store);
  }

  getPage() {
    return this.getSnapshot().number;
  }
}
