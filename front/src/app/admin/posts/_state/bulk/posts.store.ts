import { Injectable } from "@angular/core";
import { EntityState, EntityStore, StoreConfig } from "@datorama/akita";
import { Post } from "../../../../shared/models/post.model";

const initialState: PostsState = {
  content: [],
  last: false,
  totalElements: 0,
  totalPages: 0,
  size: 0,
  number: 0,
  sort: null,
  first: true,
  numberOfElements: 0,
  searchValue: null
};

export interface PostsState extends EntityState<Post> {}

@Injectable()
@StoreConfig({ name: "adm-posts" })
export class PostsStore extends EntityStore<PostsState, Post> {
  constructor() {
    super(initialState);
  }

  updatePage(page: { last: boolean; number: number }) {
    this.updateRoot(page);
  }
}
