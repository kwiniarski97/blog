import { Injectable, OnDestroy } from "@angular/core";
import { ID, transaction } from "@datorama/akita";
import { HttpClient } from "@angular/common/http";
import { PostsStore } from "./bulk/posts.store";
import { Post } from "src/app/shared/models/post.model";
import { environment } from "src/environments/environment";
import { Subject } from "rxjs";
import { takeUntil, timeout, retryWhen, delay, take } from "rxjs/operators";
import { SnackBarService } from "src/app/shared/snack-bar.service";
import { PaginatedResponse } from "src/app/shared/models/paginated-reponse.model";
import { PostsDetailStore } from "./single/post-details.store";

@Injectable()
export class PostsService implements OnDestroy {
  private destroy$ = new Subject<void>();

  private apiURL = `${environment.apiUrl}/posts/`;

  constructor(
    private postsStore: PostsStore,
    private postsDetailStore: PostsDetailStore,
    private http: HttpClient,
    private snackbar: SnackBarService
  ) {}

  ngOnDestroy() {
    this.destroy$.next();
  }

  add(post: Post) {
    this.http
      .post(`${this.apiURL}/create`, post)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        id => {
          this.snackbar.alert(`Pomyślnie dodano, id = ${id}`);
          this.postsStore.add(post);
          this.postsDetailStore.add(post);
        },
        error => {
          console.log(error);
          this.snackbar.alert(`Wystąpił błąd + ${error}`, "kurczę");
        }
      );
  }

  getPosts(page: number) {
    this.http
      .get<any>(`${this.apiURL}/recent/${page}`)
      .pipe(
        timeout(10000),
        takeUntil(this.destroy$),
        retryWhen(error => error.pipe(delay(5000)))
      )
      .subscribe(res => {
        this.updatePosts(res);
      });
  }

  update(id: number, post: Partial<Post>) {
    this.http
      .put(`${this.apiURL}/update/${id}`, post)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        () => {
          this.postsStore.upsert(id, post);
          this.postsDetailStore.upsert(id, post);
          this.snackbar.alert("Pomyślnie zaaktualizowano");
        },
        error => {
          this.snackbar.alert(error.message);
        }
      );
  }

  remove(id: ID) {
    this.http
      .delete(`${this.apiURL}/delete/${id}`)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        () => {
          this.postsStore.remove(id);
          this.postsDetailStore.remove(id);
        },
        error => {
          this.snackbar.alert(error.message);
        }
      );
  }

  getById(id: number) {
    this.http
      .get(`${this.apiURL}/${id}`)
      .pipe(takeUntil(this.destroy$))
      .subscribe((post: Post) => {
        this.postsDetailStore.add(post);
      });
  }

  @transaction()
  private updatePosts(res: PaginatedResponse<Post>) {
    const nextPage = res.number + 1;
    this.postsStore.add(res.content);
    this.postsStore.updatePage({ last: res.last, number: nextPage });
    this.postsStore.setLoading(false);
  }
}
