import { NgModule } from "@angular/core";
import { CreateComponent } from "./create/create.component";
import { SharedModule } from "src/app/shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { MatStepperModule } from "@angular/material/stepper";
import { MatSelectModule } from "@angular/material/select";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { MatCardModule } from "@angular/material/card";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { PostsQuery } from "./_state/bulk/posts.query";
import { PostCreateEditScreenComponent } from "./post-create-edit-screen/post-create-edit-screen.component";
import { PostListComponent } from "./post-list/post-list.component";
import { PostListContainerComponent } from "./post-list/post-list-container.component";
import { PostComponent } from "./post-list/post/post.component";
import { ImagePreviewComponent } from "./post-create-edit-screen/image-preview/image-preview.component";
import { TimepickerComponent } from "./post-create-edit-screen/timepicker/timepicker.component";
import { MarkupScreenComponent } from "./post-create-edit-screen/markup-screen/markup-screen.component";
import { MarkupPipe } from "./post-create-edit-screen/markup-screen/markup.pipe";
import { EditComponent } from "./edit/edit.component";
import { PostsDetailQuery } from "./_state/single/post-details.query";
import { PostsStore } from "./_state/bulk/posts.store";
import { PostsDetailStore } from "./_state/single/post-details.store";
import { PostsService } from "./_state/posts.service";
@NgModule({
  imports: [
    SharedModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatCardModule,
    InfiniteScrollModule
  ],
  declarations: [
    CreateComponent,
    PostListComponent,
    MarkupScreenComponent,
    MarkupPipe,
    ImagePreviewComponent,
    TimepickerComponent,
    PostListContainerComponent,
    PostComponent,
    PostCreateEditScreenComponent,
    EditComponent
  ],
  providers: [
    MarkupPipe,
    PostsQuery,
    PostsDetailQuery,
    PostsStore,
    PostsDetailStore,
    PostsService
  ]
})
export class PostsModule {}
