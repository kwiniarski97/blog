import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { PostsService } from "../_state/posts.service";
import { PostUpsert } from "./post-upsert.model";
import { Tag } from "src/app/shared/models/tag.model";
import { TagsService } from "../../tags/_state/tags.service";
import { TagsQuery } from "../../tags/_state/tags.query";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.scss"]
})
export class CreateComponent {
  tags$: Observable<Tag[]>;

  constructor(
    private postService: PostsService,
    private tagsStore: TagsQuery,
    private tagsService: TagsService,
  ) {
    this.getTags();
  }

  savePost(postToAdd: PostUpsert) {
    this.postService.add(postToAdd);
  }

  getTags() {
    this.tagsService.get();
    this.tags$ = this.tagsStore.selectAll();
  }
}
