import { Component, OnInit } from "@angular/core";
import {
  Router,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from "@angular/router";
import { PostsService } from "../_state/posts.service";
import { PostsQuery } from "../_state/bulk/posts.query";
import { Observable } from "rxjs";
import { Post } from "src/app/shared/models/post.model";
import { Tag } from "src/app/shared/models/tag.model";
import { TagsQuery } from "../../tags/_state/tags.query";
import { TagsService } from "../../tags/_state/tags.service";
import { PostsDetailQuery } from "../_state/single/post-details.query";
import { PostUpsert } from "../create/post-upsert.model";

@Component({
  selector: "blog-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class EditComponent {
  id: number;

  tags$: Observable<Tag[]>;

  post$: Observable<Post>;
  constructor(
    private route: ActivatedRoute,
    private postService: PostsService,
    private postQuery: PostsDetailQuery,
    private tagsQuery: TagsQuery,
    private tagsService: TagsService
  ) {
    this.getID();
    this.fetchPost();
    this.fetchTags();
  }

  edit(post: PostUpsert) {
    this.postService.update(this.id, post);
  }

  private getID(): void {
    this.id = +this.route.snapshot.paramMap.get("id");
  }

  private fetchTags(): void {
    this.tagsService.get();
    this.tags$ = this.tagsQuery.selectAll();
  }

  private fetchPost(): void {
    this.postService.getById(this.id);
    this.post$ = this.postQuery.selectEntity(this.id);
  }
}
