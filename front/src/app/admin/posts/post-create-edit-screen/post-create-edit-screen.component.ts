import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { Tag } from "src/app/shared/models/tag.model";
import { filter } from "rxjs/operators";
import { SnackBarService } from "src/app/shared/snack-bar.service";
import { PostUpsert } from "../create/post-upsert.model";
import { Post } from "src/app/shared/models/post.model";
import * as showdown from "showdown";
import * as hglt from "showdown-highlight";
import { MarkupPipe } from "./markup-screen/markup.pipe";

@Component({
  selector: "blog-post-create-edit-screen",
  templateUrl: "./post-create-edit-screen.component.html",
  styleUrls: ["./post-create-edit-screen.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCreateEditScreenComponent {
  @Input()
  tags: Tag[];
  @Output()
  submitted = new EventEmitter<PostUpsert>();

  private _post: Post;

  @Input()
  set post(post: Post) {
    //if are different
    if (post && JSON.stringify(post) !== JSON.stringify(this._post)) {
      this._post = post;
      this.generateForm();
    }
  }

  get post(): Post {
    return this._post;
  }

  form: FormGroup;
  imageUrl$: Observable<string>;
  contentText: Observable<string>;

  constructor(
    private fb: FormBuilder,
    private snackbar: SnackBarService,
    private markupPipe: MarkupPipe
  ) {
    this.generateForm();
  }

  submit() {
    if (this.form.invalid) {
      this.snackbar.alert("Popraw błędy", "OK");
      return;
    }
    const stepValues = this.form.value.steps;

    const postToAdd = new PostUpsert();

    postToAdd.title = stepValues[0].title;
    postToAdd.subtitle = stepValues[0].subtitle;
    postToAdd.tags = stepValues[0].tags;
    // TODO: create thumbnail
    postToAdd.mainImagePath = stepValues[0].image;
    postToAdd.mainImageThumbnailPath = stepValues[0].image;

    postToAdd.publishDate = Date.parse(stepValues[2].publishDate);

    const body = this.markupPipe.transform(stepValues[1].input);
    postToAdd.body = body;

    this.submitted.emit(postToAdd);
  }

  private markupify(html: string): string {
    let converter = new showdown.Converter({
      extensions: [hglt]
    });
    let md = converter.makeMarkdown(html);
    return md;
  }

  private generateForm() {
    let date: Date;
    let content: string;
    if (this.post) {
      date = new Date(this.post.publishDate);
      content = this.markupify(this.post.body);
    }

    this.form = this.fb.group({
      steps: this.fb.array([
        this.fb.group({
          title: [this.post ? this.post.title : "", Validators.required],
          subtitle: [this.post ? this.post.subtitle : "", Validators.required],
          image: [
            this.post ? this.post.mainImagePath : "",
            Validators.required
          ],
          tags: [this.post ? this.post.tags : null]
        }),
        this.fb.group({ input: [content ? content : "", Validators.required] }),

        this.fb.group({
          publishDate: [date ? date : null],
          hours: [date ? date.getHours() : 0],
          minutes: [date ? date.getMinutes() : 0]
        })
      ])
    });

    this.contentText = this.form
      .get("steps")
      .get([1])
      .get("input").valueChanges;

    const URLREGEX = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/s;
    this.imageUrl$ = this.form
      .get("steps")
      .get([0])
      .get("image")
      .valueChanges.pipe(filter(v => URLREGEX.test(v)));
  }
}
