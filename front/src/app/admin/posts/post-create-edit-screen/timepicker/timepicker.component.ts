import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { Subject } from "rxjs";

@Component({
  selector: "blog-timepicker",
  templateUrl: "./timepicker.component.html",
  styleUrls: ["./timepicker.component.scss"]
})
export class TimepickerComponent implements OnInit, OnDestroy {
  @Input()
  hours: FormControl;

  @Input()
  minutes: FormControl;

  destroy$ = new Subject();
  constructor() {}

  ngOnInit() {
    if (this.hours == null || this.minutes == null) {
      throw new Error("You must pass hours and minutes formControls");
    }
    this.preventInvalidHoursInput();
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  preventInvalidHoursInput() {
    
  }
}
