import {
  Component,
  Input,
  ChangeDetectionStrategy,
} from "@angular/core";

@Component({
  selector: "blog-image-preview",
  templateUrl: "./image-preview.component.html",
  styleUrls: ["./image-preview.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImagePreviewComponent {
  @Input()
  url: string;

  constructor() {}
}
