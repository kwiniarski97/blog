import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkupScreenComponent } from './markup-screen.component';

describe('MarkupScreenComponent', () => {
  let component: MarkupScreenComponent;
  let fixture: ComponentFixture<MarkupScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkupScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkupScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
