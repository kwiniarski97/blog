import { Pipe, PipeTransform } from "@angular/core";
import * as showdown from "showdown";
import * as hglt from "showdown-highlight";

@Pipe({
  name: "markup"
})
export class MarkupPipe implements PipeTransform {
  transform(value: string): any {
    let converter = new showdown.Converter({
      extensions: [hglt]
    });

    converter.setFlavor("github");

    let html = converter.makeHtml(value);

    return html;
  }
}
