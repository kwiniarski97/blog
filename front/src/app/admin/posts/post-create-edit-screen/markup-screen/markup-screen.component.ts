import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  selector: "app-markup-screen",
  templateUrl: "./markup-screen.component.html",
  styleUrls: ["./markup-screen.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarkupScreenComponent implements OnInit {
  @Input()
  text: string;

  constructor() {}

  ngOnInit() {}
}
