import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCreateEditScreenComponent } from './post-create-edit-screen.component';

describe('PostCreateEditScreenComponent', () => {
  let component: PostCreateEditScreenComponent;
  let fixture: ComponentFixture<PostCreateEditScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostCreateEditScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCreateEditScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
