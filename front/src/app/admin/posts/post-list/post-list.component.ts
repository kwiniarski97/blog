import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Post } from "src/app/shared/models/post.model";

@Component({
  selector: "blog-post-list",
  templateUrl: "./post-list.component.html",
  styleUrls: ["./post-list.component.scss"]
})
export class PostListComponent implements OnInit {
  @Input()
  posts: Post[];

  @Output()
  scroll = new EventEmitter<void>();

  @Output()
  delete = new EventEmitter<number>();

  @Output()
  edit = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {}

  onScroll() {
    this.scroll.emit();
  }

  onDelete(id: number): void {
    this.delete.emit(id);
  }

  onEdit(id: number): void {
    this.edit.emit(id);
  }
}
