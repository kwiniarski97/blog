import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Post } from "src/app/shared/models/post.model";
import { PostsService } from "../_state/posts.service";
import { PostsQuery } from "../_state/bulk/posts.query";
import { Router } from "@angular/router";

@Component({
  selector: "blog-post-list-container",
  template: `
    <blog-post-list
      [posts]="posts$ | async"
      (scroll)="scroll()"
      (delete)="onDelete($event)"
      (edit)="onEdit($event)"
    ></blog-post-list>
  `
})
export class PostListContainerComponent implements OnInit {
  posts$: Observable<Post[]>;
  isLoading$: Observable<boolean>;
  isLast$: Observable<boolean>;
  constructor(
    private postsQuery: PostsQuery,
    private postsService: PostsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.fetchPosts();
    this.posts$ = this.postsQuery.selectAll();
    this.isLoading$ = this.postsQuery.selectLoading();
    this.isLast$ = this.postsQuery.select(s => s.last);
  }

  scroll() {
    this.fetchPosts();
  }

  onDelete(id: number) {
    if (confirm("Czy napewno chcesz usunąć tego posta?")) {
      this.postsService.remove(id);
    }
  }

  onEdit(id: number) {
    this.router.navigate(["/admin", "posts", "edit", id]);
  }

  private fetchPosts() {
    this.postsService.getPosts(this.postsQuery.getPage());
  }
}
