import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from "@angular/core";
import { Post } from "src/app/shared/models/post.model";

@Component({
  selector: "blog-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostComponent implements OnInit {
  @Input()
  post: Post;

  @Output()
  delete = new EventEmitter<void>();

  @Output()
  edit = new EventEmitter<void>();
  constructor() {}

  ngOnInit() {}

  onEdit(): void {
    this.edit.emit();
  }
  onDelete(): void {
    this.delete.emit();
  }
}
