import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { TokenService } from "./token.service";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { tap } from "rxjs/operators";
import { Router } from "@angular/router";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public tokenService: TokenService, private router: Router) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap(
        (event: HttpEvent<any>) => {},
        error => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401) {
              this.handleUnauthorizedResponse();
            }
          }
        }
      )
    );
  }

  private handleUnauthorizedResponse() {
    this.tokenService.clearToken();
    this.router.navigate(["/"]);
  }
}
