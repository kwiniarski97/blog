import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { CoreModule } from "../../core/core.module";
import { TokenService } from "./token.service";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: CoreModule
})
export class AuthGuard implements CanActivate {
  constructor(private tokenService: TokenService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const token = this.tokenService.getCurrentToken();
    if (!token) {
      this.redirect();
      return false;
    }
    return this.tokenService.isValid(token).pipe(
      map(res => {
        if (res === false) {
          this.redirect();
        }
        return res;
      })
    );
  }

  private redirect(): void {
    this.router.navigate(["/admin", "login"]);
  }
}
