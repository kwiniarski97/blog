import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MenuComponent } from "./menu/menu.component";
import { MatMenuModule } from "@angular/material/menu";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { SharedModule } from "src/app/shared/shared.module";
import { RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";

@NgModule({
  imports: [
    CommonModule,
    MatMenuModule,
    MatToolbarModule,
    SharedModule,
    RouterModule,
    MatProgressSpinnerModule
  ],
  declarations: [MenuComponent, LoginComponent],
  exports: [MenuComponent]
})
export class CoreModule {}
