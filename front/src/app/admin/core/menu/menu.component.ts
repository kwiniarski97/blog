import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MethodFn } from "@angular/core/src/reflection/types";
import { TokenService } from "../token.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit {
  entries: MenuEntry[] = [
    new MenuEntry("Lista postów", this.navigateToPostList.bind(this)),
    new MenuEntry("Dodaj post", this.navigateToAddPost.bind(this)),
    new MenuEntry("Tagi", this.navigateToTags.bind(this))
  ];
  constructor(private router: Router, private tokenService: TokenService) {}

  ngOnInit() {}

  navigateToPostList(): void {
    this.router.navigate(["/admin", "posts", "list"]);
  }
  navigateToAddPost(): void {
    this.router.navigate(["/admin", "posts", "create"]);
  }

  navigateToTags(): void {
    this.router.navigate(["/admin", "tags"]);
  }

  logout(): void {
    this.tokenService.clearToken();
    this.router.navigate(["/"]);
  }
}

export class MenuEntry {
  label: string;
  action: MethodFn;
  disabled: boolean;

  constructor(label: string, action: MethodFn, disabled: boolean = false) {
    this.label = label;
    this.action = action;
    this.disabled = disabled;
  }
}
