import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TokenService } from "../token.service";
import { takeUntil, finalize } from "rxjs/operators";
import { Subject } from "rxjs";
import { Router } from "@angular/router";
import { SnackBarService } from "src/app/shared/snack-bar.service";

@Component({
  selector: "blog-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnDestroy {
  login: FormGroup;
  isLoading = false;
  private destroy$ = new Subject<void>();
  constructor(
    private formBuilder: FormBuilder,
    private tokenService: TokenService,
    private router: Router,
    private snackBarService: SnackBarService
  ) {
    this.login = this.formBuilder.group({
      token: ["", [Validators.required]]
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  tryLogin(): void {
    if (!this.login.valid) {
      return;
    }
    this.isLoading = true;
    const token = this.login.get("token").value;
    this.tokenService
      .isValid(token)
      .pipe(
        takeUntil(this.destroy$),
        finalize(() => (this.isLoading = false))
      )
      .subscribe(valid => {
        if (valid) {
          this.tokenService.saveToken(token);
          this.successfullLogin();
        } else {
          this.showInvalidPassword();
        }
      });
  }

  private successfullLogin(): void {
    this.snackBarService.alert("Logged successfull");
    this.router.navigate(["/admin"]);
  }

  private showInvalidPassword(): void {
    this.snackBarService.alert("Password is invalid");
  }
}
