import { Injectable } from "@angular/core";
import { Location } from "@angular/common";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { CoreModule } from "../../core/core.module";
import { TokenService } from "../token.service";

@Injectable({
  providedIn: CoreModule
})
export class AlreadyLoggedGuard implements CanActivate {
  constructor(private tokenService: TokenService, private router: Router) {}
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.tokenService.isLogged) {
      this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }
}
