import { Injectable } from "@angular/core";
import { ID } from "@datorama/akita";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { tokenKey } from "@angular/core/src/view";

@Injectable({ providedIn: "root" })
export class TokenService {
  private apiUrl = environment.apiUrl;
  private tokenKey = "token";
  public get isLogged(): boolean {
    return !!localStorage.getItem(this.tokenKey);
  }

  constructor(private http: HttpClient) {}

  public getCurrentToken(): string {
    return localStorage.getItem(this.tokenKey);
  }

  public saveToken(token: string) {
    localStorage.setItem(this.tokenKey, token);
  }

  public clearToken() {
    localStorage.removeItem(this.tokenKey);
  }

  public isValid(token: string): Observable<boolean> {
    return this.http.post<boolean>(`${this.apiUrl}/token/validate`, token);
  }
}
