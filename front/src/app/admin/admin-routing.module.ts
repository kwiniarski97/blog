import { NgModule } from "@angular/core";

import { Routes, RouterModule } from "@angular/router";
import { AdminComponent } from "./admin.component";
import { CreateComponent } from "./posts/create/create.component";
import { TagsComponent } from "./tags/tags.component";
import { AuthGuard } from "./core/auth.guard";
import { LoginComponent } from "./core/login/login.component";
import { AlreadyLoggedGuard } from "./core/login/already-logged.guard";
import { PostListContainerComponent } from "./posts/post-list/post-list-container.component";
import { EditComponent } from "./posts/edit/edit.component";

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
    canActivate: [AlreadyLoggedGuard]
  },
  {
    path: "",
    component: AdminComponent,
    children: [
      {
        path: "",
        pathMatch: "full",
        redirectTo: "posts/list"
      },
      {
        path: "posts",
        children: [
          { path: "", redirectTo: "list" },
          { path: "list", component: PostListContainerComponent },
          { path: "create", component: CreateComponent },
          { path: "edit/:id", component: EditComponent }
        ],
        canActivate: [AuthGuard]
      },
      {
        path: "tags",
        children: [{ path: "", pathMatch: "full", component: TagsComponent }],
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
