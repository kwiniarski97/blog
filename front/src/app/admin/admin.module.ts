import { NgModule } from "@angular/core";
import { AdminComponent } from "./admin.component";
import { CoreModule } from "./core/core.module";
import { AdminRoutingModule } from "./admin-routing.module";
import { PostsModule } from "./posts/posts.module";
import { TagsModule } from "./tags/tags.module";
import { SharedModule } from "../shared/shared.module";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { TokenInterceptor } from "./core/token.interceptor";

@NgModule({
  imports: [
    CoreModule,
    TagsModule,
    PostsModule,
    SharedModule
  ],
  declarations: [AdminComponent],
  exports: [AdminRoutingModule]  
})
export class AdminModule {}
