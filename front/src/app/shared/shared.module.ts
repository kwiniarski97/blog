import { NgModule } from "@angular/core";

// angular material
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { SpinnerComponent } from "./spinner/spinner.component";
import { UnsanitizePipe } from "./unsanitize.pipe";
import { CommonModule } from "@angular/common";
import { MatListModule } from "@angular/material/list";
import { RouterModule, Router } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";

// /angular material

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatListModule
  ],
  declarations: [SpinnerComponent, UnsanitizePipe],
  exports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    SpinnerComponent,
    UnsanitizePipe,
    MatListModule
  ]
})
export class SharedModule {}
