import { ID } from "@datorama/akita";
import { Tag } from "./tag.model";

export class Post {
  _id: ID;
  id: number;
  title: string;
  subtitle: string;
  mainImagePath: string;
  mainImageThumbnailPath: string;
  body: string;
  creationDate: number;
  publishDate: number;
  views: number;
  score: number;
  tags: Tag[]
};
