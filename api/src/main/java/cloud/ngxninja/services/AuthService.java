package cloud.ngxninja.services;

import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class AuthService {


    public String getPasswordHash(String password, String saltHash) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        Mac sha512_HMAC;

        var byteKey = saltHash.getBytes("UTF-8");
        final var HMAC_SHA512 = "HmacSHA512";
        sha512_HMAC = Mac.getInstance(HMAC_SHA512);
        var keySpec = new SecretKeySpec(byteKey, HMAC_SHA512);
        sha512_HMAC.init(keySpec);
        var mac_data = sha512_HMAC.
                doFinal(password.getBytes("UTF-8"));
        return bytesToHex(mac_data);

    }

    private static String bytesToHex(byte[] bytes) {
        final var hexArray = "0123456789ABCDEF".toCharArray();
        var hexChars = new char[bytes.length * 2];
        for (var j = 0; j < bytes.length; j++) {
            var v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
