package cloud.ngxninja.services;

import cloud.ngxninja.models.dtos.PostRankingDTO;
import cloud.ngxninja.models.dtos.RankingDTO;
import cloud.ngxninja.models.dtos.TagRankingDTO;
import cloud.ngxninja.repository.PostRepository;
import cloud.ngxninja.repository.TagRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class RankingService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private TagRepository tagsRepository;
    @Autowired
    DozerBeanMapper mapper;

    @Autowired
    EntityManager entityManager;


    public RankingDTO getRanking() {

        var ranking = new RankingDTO();

        ranking.mostPopularTags = getMostPopularTags();
        ranking.mostPopularPosts = getMostViewedPosts();
        ranking.bestRatedPosts = getBestRatedPosts();
        return ranking;
    }

    private List<TagRankingDTO> getMostPopularTags() {
        var query = entityManager.createNativeQuery("select t.id as 'id', t.name as 'name', count(t.id) as ile from tag as t left join post_tag as pt on t.id = pt.tag_id left join post as p on pt.post_id = p.id group by t.id order by ile desc limit 5");
        var result = query.getResultList();
        ArrayList output = new ArrayList<TagRankingDTO>();
        for (var obj : result) {
            var mapped = (Object[]) obj;
            var dest = new TagRankingDTO();
            dest.id = ((BigInteger) mapped[0]).longValue();
            dest.name = (String) mapped[1];
            output.add(dest);
        }
        return output;
    }

    private List<PostRankingDTO> getMostViewedPosts() {
        var query = entityManager.createNativeQuery("select p.id as 'id', p.title, p.views as count from post as p order by count desc limit 5");
        var queryResults = query.getResultList();
        ArrayList output = new ArrayList<PostRankingDTO>();
        mapPostResults(queryResults, output);
        return output;
    }

    private List<PostRankingDTO> getBestRatedPosts(){
        var query = entityManager.createNativeQuery("select p.id, p.title, (p.score / p.views) as ratio from post as p order by ratio desc limit 5");
        var queryResults = query.getResultList();
        ArrayList output = new ArrayList<PostRankingDTO>();
        mapPostResults(queryResults, output);
        return output;
    }

    private void mapPostResults(List queryResults, ArrayList<PostRankingDTO> output) {
        for (var obj : queryResults){
            var mapped = (Object[]) obj;
            var dest = new PostRankingDTO();
            dest.id = ((BigInteger) mapped[0]).longValue();
            dest.title = (String) mapped[1];
            output.add(dest);
        }
    }
}
