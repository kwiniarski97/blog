package cloud.ngxninja.services;

import cloud.ngxninja.models.domain.Post;
import cloud.ngxninja.models.domain.Tag;
import cloud.ngxninja.models.dtos.PostCreateDTO;
import cloud.ngxninja.models.dtos.PostDetailDTO;
import cloud.ngxninja.models.dtos.PostReducedDTO;
import cloud.ngxninja.repository.TagRepository;
import cloud.ngxninja.repository.PostRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

@Service
public class PostService {

    private final int PAGE_SIZE = 10;

    @Autowired
    private PostRepository repository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    DozerBeanMapper mapper;

    /**
     * @param page 1 - indexed
     */
    public Page<PostReducedDTO> getLatest(int page) {
        Pageable request = new PageRequest(page, PAGE_SIZE, Sort.Direction.DESC, "publishDate");
        var entities = repository.findByPublishDateIsNotNullAndDeletedIsFalse(request);
        return entities.map(entity -> {
            var dest = new PostReducedDTO();
            mapper.map(entity, dest);
            return dest;
        });
    }

    public long create(PostCreateDTO postCreate) {
        var post = mapper.map(postCreate, Post.class);
        post.setCreationDate(getCurrentTimeStamp());
        var obj = this.repository.save(post);
        this.repository.flush();
        return obj.getId();
    }

    public void publish(long id) {
        var post = repository.findOne(id);
        post.setPublishDate(getCurrentTimeStamp());
        this.repository.flush();
    }

    public void delete(long id) {
        var post = repository.findOne(id);
        post.setDeleted(true);
        this.repository.flush();
    }


    private Timestamp getCurrentTimeStamp() {
        return new Timestamp(new Date().getTime());
    }

    public void update(long id, PostCreateDTO createDTO) {
        var post = repository.findOne(id);
        mapper.map(createDTO, post);
        repository.save(post);
    }

    public PostDetailDTO getById(long id) {
        var post = repository.findOne(id);
        return mapper.map(post, PostDetailDTO.class);
    }

    public Page<PostReducedDTO> getByTagId(long tagId, int page) {
        var tag = tagRepository.findOne(tagId);
        Pageable request = new PageRequest(page, PAGE_SIZE, Sort.Direction.DESC, "publishDate");
        var entities = repository.findByTagsContains(tag, request);
        return entities.map(entity -> {
            var dest = new PostReducedDTO();
            mapper.map(entity, dest);
            return dest;
        });
    }

    public Page<PostReducedDTO> searchByQuery(String query, int page) {
        Pageable request = new PageRequest(page, PAGE_SIZE, Sort.Direction.DESC, "publishDate");

        var posts = repository.findPostsByTitleContainsOrSubtitleContains(query, query, request);

        return posts.map(entity -> {
            var dest = new PostReducedDTO();
            mapper.map(entity, dest);
            return dest;
        });
    }

    public long upVote(long id) {
        var post = repository.findOne(id);
        post.setScore(post.getScore() + 1);
        repository.flush();
        return post.getScore();
    }
}
