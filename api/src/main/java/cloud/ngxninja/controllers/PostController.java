package cloud.ngxninja.controllers;

import cloud.ngxninja.models.dtos.PostCreateDTO;
import cloud.ngxninja.models.dtos.PostDetailDTO;
import cloud.ngxninja.models.dtos.PostReducedDTO;
import cloud.ngxninja.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/posts")
public class PostController extends ControllerBase {

    @Autowired
    private PostService postService;

    @RequestMapping("/recent/{page}")
    public Page<PostReducedDTO> getRecent(@PathVariable(value = "page") int page) {
        return postService.getLatest(page);
    }

    @GetMapping("/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public PostDetailDTO getById(@PathVariable(value = "id") long id) {
        return postService.getById(id);
    }

    @GetMapping("/tag/{tagId}/{page}")
    @ResponseBody
    public Page<PostReducedDTO> getByTagId(@PathVariable(value = "tagId") long tagId, @PathVariable(value = "page") int page) {
        return postService.getByTagId(tagId, page);
    }

    @GetMapping("/search/{page}")
    public Page<PostReducedDTO> searchByQuery(@PathVariable(value = "page") int page, @RequestParam(value = "query") String query) {
        return postService.searchByQuery(query, page);
    }

    @PostMapping("/upvote/{id}")
    public ResponseEntity upvote(@PathVariable(value = "id") long id){
        try{
            var newScore = postService.upVote(id);
            return ResponseEntity.ok(newScore);
        } catch (Exception ex){
            return ResponseEntity.badRequest().build();
        }
    }

    //AUTHORIZED
    @PostMapping("/create")
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    public ResponseEntity<Long> create(@RequestBody PostCreateDTO post, @RequestHeader(value = "token", required = false) String token) {
        if (isValidToken(token)) {
            return ResponseEntity.ok(postService.create(post));
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @PutMapping("/publish/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity publish(@PathVariable(value = "id") long id, @RequestHeader(value = "token", required = false) String token) {
        if(isValidToken(token)){
            postService.publish(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity delete(@PathVariable(value = "id") long id, @RequestHeader(value = "token", required = false) String token) {
        if(isValidToken(token)){
            postService.delete(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @PutMapping("/update/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity update(@PathVariable(value = "id") long id, @RequestBody PostCreateDTO post, @RequestHeader(value = "token", required = false) String token) {
        if(isValidToken(token)){
            postService.update(id, post);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
    // /AUTHORIZED


}
