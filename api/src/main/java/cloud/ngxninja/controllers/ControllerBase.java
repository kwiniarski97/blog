package cloud.ngxninja.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;

public abstract class ControllerBase {

    @Autowired
    private Environment env;

    protected boolean isValidToken(String token){
        var adminToken = env.getProperty("adminToken");
        if(adminToken.equals(token)){
            return true;
        } else{
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return  false;
        }
    }

}
