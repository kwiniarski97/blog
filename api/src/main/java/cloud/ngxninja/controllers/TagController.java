package cloud.ngxninja.controllers;

import cloud.ngxninja.models.dtos.TagDTO;
import cloud.ngxninja.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/tags")
public class TagController extends ControllerBase {

    @Autowired
    TagService service;

    @GetMapping("/")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public List<TagDTO> getTags() {
        return service.getTags();
    }

    @PostMapping("/create")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<Long> create(@RequestBody TagDTO tag, @RequestHeader(value = "token", required = false) String token) {
        if (isValidToken(token)) {
            return ResponseEntity.ok(service.create(tag));
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public ResponseEntity delete(@PathVariable(value = "id") long id, @RequestHeader(value = "token", required = false) String token) {
        if (isValidToken(token)) {
            try {
                service.delete(id);
                return ResponseEntity.ok().build();
            } catch (Exception ex){
                return ResponseEntity.badRequest().build();
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
