package cloud.ngxninja.controllers;

import cloud.ngxninja.models.dtos.RankingDTO;
import cloud.ngxninja.services.RankingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/rankings")
public class RankingController extends ControllerBase {

    @Autowired
    private RankingService rankingService;

    @GetMapping()
    @ResponseStatus(value = HttpStatus.OK)
    public RankingDTO getRanking() {
        return rankingService.getRanking();
    }
}
