package cloud.ngxninja.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/token")
public class TokenController extends ControllerBase {

    @PostMapping("/validate")
    public ResponseEntity<Boolean> isValid(@RequestBody String token) {
        return ResponseEntity.ok(isValidToken(token));
    }

}
