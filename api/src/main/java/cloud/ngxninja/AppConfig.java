package cloud.ngxninja;

import cloud.ngxninja.models.domain.Post;
import cloud.ngxninja.models.domain.Tag;
import cloud.ngxninja.models.dtos.*;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AppConfig {

    @Bean(autowire = Autowire.BY_TYPE, name = "Mapper")
    public DozerBeanMapper MapperConfig() {
        var d = new DozerBeanMapper();
        d.addMapping(new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(PostCreateDTO.class, Post.class);
                mapping(PostReducedDTO.class, Post.class);
                mapping(Tag.class, TagDTO.class);
                mapping(PostDetailDTO.class, Post.class);
            }
        });


        return d;
    }
}

