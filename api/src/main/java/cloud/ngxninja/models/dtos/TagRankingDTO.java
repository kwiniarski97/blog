package cloud.ngxninja.models.dtos;

import lombok.Getter;
import lombok.Setter;

public class TagRankingDTO {
    @Getter
    @Setter
    public long id;

    @Getter
    @Setter
    public String name;

}
