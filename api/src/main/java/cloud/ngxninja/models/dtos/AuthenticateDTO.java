package cloud.ngxninja.models.dtos;

import lombok.Getter;
import lombok.Setter;

public class AuthenticateDTO {
    @Getter
    @Setter
    public String email;
    @Getter
    @Setter
    public String password;
}
