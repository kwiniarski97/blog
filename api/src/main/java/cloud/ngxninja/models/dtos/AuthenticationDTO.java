package cloud.ngxninja.models.dtos;

import lombok.Getter;
import lombok.Setter;

public class AuthenticationDTO {
    public String token;
}
