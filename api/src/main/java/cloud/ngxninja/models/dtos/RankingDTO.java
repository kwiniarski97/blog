package cloud.ngxninja.models.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class RankingDTO {
    @Getter
    @Setter
    public List<TagRankingDTO> mostPopularTags;
    @Getter
    @Setter
    public List<PostRankingDTO> mostPopularPosts;
    @Getter
    @Setter
    public List<PostRankingDTO> bestRatedPosts;
}


