package cloud.ngxninja.models.domain;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

public class Token {
    @Getter
    @Setter
    private Date issuedAt;
    private String token;

}
