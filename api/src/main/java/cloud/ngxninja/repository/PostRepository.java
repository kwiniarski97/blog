package cloud.ngxninja.repository;

import cloud.ngxninja.models.domain.Post;
import cloud.ngxninja.models.domain.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    // great job creating query by name best idea ever 😖
    Page<Post> findByPublishDateIsNotNullAndDeletedIsFalse(Pageable pageable);

    Page<Post> findByTagsContains(Tag tag, Pageable pageable);

    Page<Post> findPostsByTitleContainsOrSubtitleContains(String query1, String query2, Pageable pageable);
}
