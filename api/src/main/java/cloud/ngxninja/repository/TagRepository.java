package cloud.ngxninja.repository;

import cloud.ngxninja.models.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {

}
